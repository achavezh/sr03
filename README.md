# Devoir 1 SR03
## Membres:
- CARREZ Emilien
- CHÁVEZ HERREJÓN Andrea

## Tutoriel installation du git en local 
##### Pour télécharger:
tapez les commandes successives dans le repertoire de votre choix:
```bash
git clone git@gitlab.utc.fr:achavezh/sr03.git
```
```bash
cd sr03 											#on se place dans le répertoire git
```

##### Pour télécharger les mises à jour (il faut le faire avant toute modification)
```bash 
git pull
```
##### Pour mettre à jour ses modifications
```bash
git commit -a -m "message associé au commit"        # commit = sauvegarder en local (l'option -a veut dire all)
git push 											# pour uploader tous le code modifié (commited)
```

##### Pour ajouter tous les nouveaux fichiers créés localement au projet
```bash
git add * 										    # à faire avant le nouveau commit
```
## Tutoriel d'utilisation
##### Pour importer le projet sur Eclipse
```bash
File > Import > Existing Projects into Workspace
Après on se place dans le repertoire du projet
```
##### Pour compiler et run le projet
```bash
On va compiler et run "as java application" premièrement la classe du serveur
Java Resources > src > Serveur > Class_Serv.java

Après on compile et run "as java application" la classe du client
Java Resources > src > Client > Class_Client.java
```

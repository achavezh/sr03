package General;

import java.net.Socket;

import Client.MessageReceptor_client_Envoyer;
import Client.MessageReceptor_client_Recevoir;
import Serveur.MessageReceptor_Server;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Message_Receptor est une classe abstraite heritant de Thread et
 *         utilisee comme modele pour le thread serveur et les threads clients
 *         </b>
 *         </p>
 *         <p>
 *         Un thread est represente par l'information suivante: Une socket de
 *         connexion/communication avec le serveur.
 *         </p>
 * @see MessageReceptor_client_Envoyer
 * @see MessageReceptor_client_Recevoir
 * @see MessageReceptor_Server
 */

public abstract class MessageReceptor extends Thread {

	/**
	 * L'attribut com est une socket permettant la connexion/communication avec le
	 * serveur.
	 * 
	 * @see MessageReceptor#getSocket()
	 * @see MessageReceptor#setSocket(Socket)
	 */
	protected Socket com;

	/**
	 * Constructeur de la classe MessageReceptor.
	 * 
	 * @param com Socket de connexion/communication avec le serveur.
	 */

	public MessageReceptor(Socket com) {
		this.com = com;
	}

	/**
	 * Retourne la socket de communication client-serveur.
	 * 
	 * @return La socket de communication client-serveur.
	 */
	public Socket getSocket() {
		return com;
	}

	/**
	 * Change la valeur de l'attribut {@link MessageReceptor#com}.
	 * 
	 * @param s Socket de communication client-serveur.
	 */
	public void setSocket(Socket s) {
		com = s;
	}

	/**
	 * Fonction run() utilisee pour definir le traitement effectue par un thread
	 * MessageReceptor en execution.
	 */

	public abstract void run();

}

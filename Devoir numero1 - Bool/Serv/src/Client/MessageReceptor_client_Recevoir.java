package Client;

import java.net.*;
import java.io.*;

import General.MessageReceptor;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Message_Receptor_client_Recevoir est une classe thread heritant
 *         de MessageReceptor et qui a pour but de recevoir et afficher les
 *         messages provenant du serveur. </b>
 *         </p>
 *         <p>
 *         Ce thread differe des autres par son traitement en execution.
 *         </p>
 */

public class MessageReceptor_client_Recevoir extends MessageReceptor {

	/**
	 * Constructeur de la classe MessageReceptor_client_Recevoir.
	 * 
	 * @param serv Socket de connexion/communication avec le serveur.
	 */
	public MessageReceptor_client_Recevoir(Socket serv) {
		super(serv);
	}

	/**
	 * L'attribut running est le boolean controlant l'execution du thread.
	 * {@link MessageReceptor_client_Recevoir}.
	 * 
	 * @see MessageReceptor_client_Recevoir#getRunning()
	 * @see MessageReceptor_client_Recevoir#setRunning(boolean)
	 */
	private volatile boolean running = true; // volatile permet d'�crire dans la memoire principale et pas sur le
												// cache
	// des threads et donc de garder une coherence sur la valeur de la
	// variable tant qu'un seul thread �crit dessus et que l'autre ne fait
	// que de lire.

	/**
	 * L'attribut in est l'entr�e de la socket communication client-serveur.
	 * 
	 * @see MessageReceptor_client_Recevoir#getIn()
	 * @see MessageReceptor_client_Recevoir#setIn(DataInputStream)
	 */

	private DataInputStream in;

	/**
	 * Retourne l'entree de la socket de communication client-serveur.
	 * 
	 * @return L'entree de la socket de communication client-serveur.
	 */
	public DataInputStream getIn() {
		return in;
	}

	/**
	 * Retourne le boolean controlant l'execution.
	 * 
	 * @return Le boolean controlant l'execution.
	 */
	public boolean getRunning() {
		return running;
	}

	/**
	 * Change la valeur de l'attribut {@link MessageReceptor_client_Recevoir#in}.
	 * 
	 * @param i Entree de la socket de communication client-serveur.
	 */
	public void setIn(DataInputStream i) {
		in = i;
	}

	/**
	 * Change la valeur de l'attribut running.
	 * 
	 * @param r Boolean controlant l'execution du thread.
	 */
	public void setRunning(boolean r) {
		running = r;
	}

	/**
	 * Fonction run() utilisee pour definir le traitement effectue par un thread
	 * MessageReceptor_client_Recevoir en ex�cution.
	 * <p>
	 * Ce traitement consiste à faire la suite d'instruction suivante en boucle tant
	 * que la condition "doit continuer a s'executer" est remplie:
	 * <ul>
	 * <li>Attendre la reception d'un message sur l'entree de lecture de la socket
	 * de communication pendant un certain temps defini.</li>
	 * <li>Afficher ce message sur la sortie standard.</li>
	 * </ul>
	 * <p>
	 * Pour ce qui est de la gestion de la fermeture du thread , si la condition
	 * "doit continuer a s'executer" n'est plus remplie, celui-ci s'arrete
	 * proprement. Cependant, si le serveur s'interrompt sans prevenir, la connexion
	 * entre le client et le serveur (socket) se ferme (parce que le serveur
	 * s'interrompt) ce qui provoque une exception de type SocketException et
	 * termine l'ex�cution du thread {@link MessageReceptor_client_Recevoir}.
	 * </p>
	 */
	public void run() {
		try {
			in = new DataInputStream(com.getInputStream());
			com.setSoTimeout(1000);
			while (running && !com.isClosed() && com.isConnected()) {
				try {
					String chaine = in.readUTF();// attendre un message en provenance du serveur.
					System.out.println(chaine);// afficher le message
				} catch (SocketTimeoutException to) {
					continue;
				} // exception lancee au bout de 1000 ms lorsque rien n'est lu sur l'entree de la
					// socket, permet au programme de boucler et continuer d'aller verifier la
					// variable running (condition d'execution "doit continuer a s'executer"
			}
		} catch (SocketException s) {
			System.out.println("Connexion perdue avec le serveur, vous allez être déconnecté...");
			// si la connexion entre le serveur et le client se ferme brusquement (ex:
			// serveur se ferme brusquement), lire sur l'entree getIn() provoque une
			// exception SocketException, on eteint l'autre thread client par le biais de
			// closeConnexion() si la socket n'est pas deja fermee.
		} catch (IOException ex) {
			// si la connexion entre le serveur et le client se ferme brusquement (ex:
			// serveur se ferme brusquement) ou que l'on a un probleme au niveau de l'entree
			// de la socket, on eteint l'autre thread client par le biais de
			// closeConnexion().
		}
		System.out.println("Deconnexion...");
		Class_Client.closeConnexion();// lancer la fonction de fermeture du client.
	}

}

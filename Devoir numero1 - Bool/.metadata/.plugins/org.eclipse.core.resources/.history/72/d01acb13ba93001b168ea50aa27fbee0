package Serveur;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Class_Serv est la classe représantant l'entité serveur du chat
 *         multithread, qui se divise en un thread serveur traitant chaque
 *         client connecté </b>
 *         </p>
 *         <p>
 *         Un serveur est représenté par les informations suivantes : Une
 *         HashMap associant un pseudo Client avec une socket de communication
 *         connectant le client en question et le serveur.
 *         </p>
 *         <p>
 *         Le serveur est implémenté directement à travers sa fonction main qui
 *         crée la socket, les threads serveur et accepte les connexions.
 *         </p>
 * @see MessageReceptor_Server
 */

public class Class_Serv {

	/**
	 * L'attribut clients est la HashMap associant le pseudo client et la socket de
	 * communication connectant le serveur avec le client en question. Cette
	 * structure permet notamment d'éviter deux pseudos identiques.
	 * 
	 * @see Class_Serv#getClients()
	 * @see Class_Serv#setClients(HashMap)
	 */
	private static HashMap<String, Socket> clients = new HashMap<String, Socket>();

	/**
	 * L'attribut socket serveur.
	 * 
	 * @see Class_Serv#getServSock()
	 * @see Class_Serv#setServSock(ServerSocket)
	 */
	private ServerSocket com;

	/**
	 * Constructeur de la classe Class_Serv.
	 */
	public Class_Serv() {
	}

	/**
	 * Retourne la socket serveur.
	 * 
	 * @return La socket serveur.
	 */
	public static ServerSocket getServSock() {
		return com;
	}

	/**
	 * Change la valeur de l'attribut {@link Class_Serv#com}.
	 * 
	 * @param c Socket serveur du serveur en cours.
	 */
	public static void setServSock(ServerSocket c) {
		com = c;
	}

	/**
	 * Retourne une HashMap contenant la liste des clients associés à leur pseudo.
	 * 
	 * @return une HashMap contenant la liste des clients associés à leur pseudo.
	 */
	public static HashMap<String, Socket> getClients() {
		return clients;
	}

	/**
	 * Change la valeur de l'attribut {@link Class_Serv#clients}.
	 * 
	 * @param c HashMap contenant la liste des clients associés à leur pseudo.
	 */
	public static void setClients(HashMap<String, Socket> c) {
		clients = c;
	}

	/**
	 * Permet de diffuser une chaine de caractère à un ou plusieurs clients.
	 * 
	 * @param chaine Chaine de caractère à diffuser aux clients.
	 * @param cli    Si cli=null on envoie la chaine à tous les clients, sinon on
	 *               l'envoie au client spécifié (lui-même le plus souvent).
	 * 
	 */

	public static void diffusion(String chaine, Socket cli) {
		if (cli == null) { // on envoie le message à tous les clients
			for (Map.Entry<String, Socket> m : clients.entrySet()) {
				try {
					DataOutputStream out = new DataOutputStream(m.getValue().getOutputStream());
					out.writeUTF(chaine);// on écrit la chaine sur la sortie de la socket de communication
				} catch (IOException ex) {// exception levée si le communication n'est plus établie ou que la sortie
											// n'est pas récupérable
					System.out.println("Problème survenu sur le serveur...");
				}
			}
		} else {
			try {
				DataOutputStream out = new DataOutputStream(cli.getOutputStream());
				out.writeUTF(chaine);
			} catch (IOException e) { // exception levée si le communication n'est plus établie ou que la sortie n'est
										// pas récupérable
				System.out.println("Problème survenu sur le serveur...");
			}
		}

	}

	/**
	 * Permet de libérer la connexion et communication entre le client donné en
	 * paramètre et le serveur.
	 * 
	 * @param cli    Socket de communication client-serveur avec lequel on veut
	 *               fermer la connexion.
	 * @param pseudo Pseudo du client avec lequel on veut fermer la connexion.
	 */

	public static void kill(Socket cli, String pseudo) {
		if (clients.remove(pseudo, cli)) { // si le client est dans le tableau (son pseudo a été accepté), on le retire
			System.out.println("Client enlevé du tableau");
			diffusion("L'utilisateur " + pseudo + " a quitté la conversation", null);// on diffuse un message
																						// prévenant les autres
																						// utilisateurs que le client se
																						// déconnecte
		}
		try {
			if (cli.isConnected()) {
				cli.close();
				System.out.println("Kill client " + pseudo);
			}
		} catch (IOException e) {// exception catch si l'on arrive pas à deconnecter le client
			System.out.println("Problème survenu sur le serveur...");
		}
	}

	/**
	 * Permet d'ajouter le client dans la Hashmap {@link Class_Serv#clients} si le
	 * pseudo choisi n'a pas déjà été utilisé, ceci permet de valider définitivement
	 * la connexion du client. Il renvoie vrai si le pseudo du client est accepté et
	 * faux sinon.
	 * 
	 * @param log Pseudo choisi par le client.
	 * @param com Socket de communication client-serveur.
	 * @return Faux si le client existe déjà (même pseudo) et vrai sinon.
	 */

	public static boolean check_Id(String log, Socket com) {
		if (clients.containsKey(log))
			return false;
		clients.put(log, com);
		return true;

	}

	/**
	 * Fonction principale permettant la création de la socket, l'acceptation des
	 * connexions clientes et la création/lancement des threads serveur desservant
	 * un client en particulier.
	 * 
	 * @see MessageReceptor_Server
	 * @param args Argument que l'on peut passer au main (aucun ici)/
	 */
	public static void main(String[] args) {// dans le main on ne rajoute pas dans la Hashmap le client car on attend
											// que celui-ci s'identifie (voir dans le thread)
		try {
			com = new ServerSocket(20000);
			System.out.println("Création du serveur...");
			while (true) {
				Socket con = com.accept(); // on accepte les clients voulant se connecter
				MessageReceptor_Server msgreceptor = new MessageReceptor_Server(con);// on crée le thread serveur
																						// desservant le client accepté
				msgreceptor.start();// on lance le thread
				System.out.println("Demande de connexion acceptée....");
			}
		} catch (IOException e) { // socket impossible à créer ou problème de connexion entre client/serveur
			System.out.println("Problème survenu sur le serveur...");
			try {
				com.close();
			} catch (IOException ex) {
			}
		}

	}

}

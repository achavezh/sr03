package Serveur;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import Client.Class_Client;
import General.MessageReceptor;

/**
 * 
 * @author C-Emilen <p><b> Message_Receptor_server est une classe thread h�ritant
 *         de MessageReceptor et qui a pour but de r�cup�rer les messages
 *         envoy�s par le client qu'il sert et de les renvoyer aux autres
 *         clients. </b></p>
 *         <p>
 *         Ce thread diff�re des autres par son traitement en ex�cution, mais
 *         aussi de par ses nouveaux attributs:
 *         <ul>
 *         <li>Un attribut pseudo qui est en fait celui du client que le thread
 *         serveur sert.</li>
 *         <li>L'entr�e de la socket de communication entre le client et le
 *         thread serveur.</li>
 *         </ul>
 */

public class MessageReceptor_server extends MessageReceptor {
	
	/**
	 * Constructeur de la classe MessageReceptor_server.
	 * 
	 * @param com Socket de connexion/communication entre le client et
	 *             le thread serveur.
	 */
	public MessageReceptor_server(Socket com) {
		super(com);
	}

	/**
	 * L'attribut pseudo est le pseudo du client que le thread dessert.
	 */
	private String pseudo = "";

	/**
	 * L'attribut ins est l'entr�e de la socket de communication permettant de lire
	 * les messages venant du client.
	 */
	private DataInputStream ins;
	
	/**
	 * Retourne le pseudo du client.
	 * @return Le pseudo du client.
	 */
	
	public String getPseudo() {
		return pseudo;
	}
	
	/**
	 * Change la valeur de l'attribut {@link MessageReceptor_server#pseudo}.
	 * @param p Pseudo du client.
	 */
	
	public void setPseudo(String p) {
		pseudo=p;
	}
	
	/**
	 * Retourne le pseudo du client.
	 * @return L'entr�e de la socket de communication.
	 */
	
	public DataInputStream getIn() {
		return ins;
	}

	/**
	 * Fonction run() utilis�e pour d�finir le traitement effectu� par un thread
	 * MessageReceptor_server en ex�cution.
	 * <p>
	 * Ce traitement consiste � faire plusieurs suite d'instruction r�parties dans
	 * deux boucles tant que le thread n'est pas interrompu.
	 * <ul>
	 * <li>Premi�re boucle: La boucle de la connexion
	 * <ul>
	 * <li>Demander le pseudo du client.</li>
	 * <li>Si le pseudo est �gale � exit, on lib�re les ressources de communication
	 * entre le thread serveur et le client puis on termine l'ex�cution.</li>
	 * <li>Si le client n'a rien rentr�, on lui envoie un message et on r�it�re la
	 * boucle.</li>
	 * <li>Si le pseudo est d�j� pris, on lui envoie un message et on r�it�re la
	 * boucle.</li>
	 * <li>Si le pseudo est valide, on diffuse un message pour signaler une nouvelle
	 * connexion � tous les clients et on valide d�finitivement sa connexion
	 * avec {@link Class_Serv#check_Id(String, Socket)}
	 * </ul>
	 * <li>Deuxi�me boucle: La boucle de diffusion des messages
	 * <ul>
	 * <li>Le thread attend un message du client.</li>
	 * <li>Si le message est �gale � exit, on lib�re les ressources de communication
	 * entre le thread serveur et le client puis on termine l'ex�cution.</li>
	 * <li>Sinon, on diffuse le message � tous les clients.</li>
	 * </ul>
	 * </ul>
	 */

	public void run() {
		try {
			ins = new DataInputStream(com.getInputStream());
			boolean identify = true;
			String chaine = "";
			while (identify) { // tant que l'utilisateur ne s'est pas connect� ou n'a pas exit
				Class_Serv.diffusion("Entrez votre pseudo:", com);
				pseudo = ins.readUTF();
				if (pseudo.equals("exit")) {// si le pseudo est exit on quitte le thread et on lib�re les ressources
					Class_Serv.kill(com, pseudo);
					break;
				} else {
					if (pseudo.equals("")) // si le pseudo n'est pas viable on envoie un message signal et on r�it�re la
											// demande
						Class_Serv.diffusion("Le pseudo est vide", com);
					else {
						if (Class_Serv.check_Id(pseudo, com)) { // si le pseudo est viable et unique on le rajoute � la
																// HashMap et on pr�vient les autres clients.
							Class_Serv.diffusion("  " + pseudo + " a rejoint la conversation", null);
							Class_Serv.diffusion("------------------------------", com);
							identify = false;
						} else
							Class_Serv.diffusion("Pseudo d�j� pris", com); // si le pseudo est d�j� pris on envoie un
																			// message signal et on r�it�re la demande
					}
				}
			}
			if (identify == false) { // si l'utilisateur ne s'est pas identifi� et a pr�f�r� exit, on ne va pas dans
										// la boucle
				while (true) {
					chaine = ins.readUTF();// on attend un message du client
					if (chaine.equals("exit")) { // si le message est exit on lib�re les ressources et on exit
						Class_Serv.kill(com, pseudo);
						break;
					} else
						Class_Serv.diffusion(pseudo + " a dit: " + chaine, null); // sinon on diffuse le message
				}
			}
		} catch (SocketException s) {
		} catch (EOFException e) {
		} catch (IOException ex) {
		}
		try {
			ins.close(); // on lib�re l'entr�e de la socket de communication
		} catch (IOException ex) {
			System.out.println("Probl�me de connexion avec le client...");
		}
		Class_Serv.kill(com, pseudo); // on lib�re les autres ressources utilis�es pour la communication.
	}

}

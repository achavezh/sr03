package Serveur;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import Client.Class_Client;
import Client.MessageReceptor_client_Envoyer;
import Client.MessageReceptor_client_Recevoir;

/**
 * 
 * @author C-Emilen <p><b> Class_Serv est la classe repr�santant l'entit� serveur
 *         du chat multithread, qui se divise en un thread serveur traitant
 *         chaque client connect� </b></p>
 *         <p>
 *         Un serveur est repr�sent� par les informations suivantes : Une
 *         HashMap associant un pseudo Client avec une socket de communication
 *         connectant le client en question et le serveur.
 *         </p>
 *         <p>
 *         Le serveur est impl�ment� directement � travers sa fonction main qui
 *         cr�e la socket, les threads serveur et accepte les connexions.
 *         </p>
 * @see MessageReceptor_server
 */

public class Class_Serv {
	
	/**
	 * Constructeur de la classe Class_Serv.
	 */
	public Class_Serv(){}

	/**
	 * L'attribut clients est la HashMap associant le pseudo client et la socket de
	 * communication connectant le serveur avec le client en question.
	 */
	private static HashMap<String, Socket> clients = new HashMap<String, Socket>();
	
	/**
	 * Retourne une HashMap contenant la liste des clients associ�s � leur pseudo.
	 * @return une HashMap contenant la liste des clients associ�s � leur pseudo.
	 */
	public static HashMap<String,Socket> getClients(){
		return clients;
	}
	
	public static void setClients(HashMap<String, Socket> c){
		clients=c;
	}
	
	/**
	 * Permet de diffuser une chaine de caract�re � un ou plusieurs clients.
	 * 
	 * @param chaine Chaine de caract�re � diffuser aux clients
	 * @param cli    Si cli=null on envoie la chaine � tous les clients, sinon on
	 *               l'envoie au client sp�cifi� (lui-m�me le plus souvent).
	 * 
	 */

	public static void diffusion(String chaine, Socket cli) {
		if (cli == null) { // on envoie le message � tous les clients
			for (Map.Entry<String, Socket> m : clients.entrySet()) {
				try {
					DataOutputStream out = new DataOutputStream(m.getValue().getOutputStream());
					out.writeUTF(chaine);// on �crit la chaine sur la sortie de la socket de communication
				} catch (IOException ex) {// exception lev�e si le communication n'est plus �tablie ou que la sortie
											// n'est pas r�cup�rable
					System.out.println("Probl�me survenu sur le serveur...");
				}
			}
		} else {
			try {
				DataOutputStream out = new DataOutputStream(cli.getOutputStream());
				out.writeUTF(chaine);
			} catch (IOException e) { // exception lev�e si le communication n'est plus �tablie ou que la sortie n'est
										// pas r�cup�rable
				System.out.println("Probl�me survenu sur le serveur...");
			}
		}

	}

	/**
	 * Permet de lib�rer la connexion et communication entre le client donn� en
	 * param�tre et le serveur.
	 * 
	 * @param cli    Socket de communication reliant le serveur et le client avec
	 *               lequel on veut fermer la connexion.
	 * @param pseudo Pseudo du client avec lequel on veut fermer la connexion.
	 */

	public static void kill(Socket cli, String pseudo) {
		if (clients.remove(pseudo, cli)) { // si le client est dans le tableau (son pseudo a �t� accept�), on le retire
			System.out.println("Client enlev� du tableau");
			diffusion("L'utilisateur " + pseudo + " a quitt� la conversation\n", null);// on diffuse un message
																						// pr�venant les autres
																						// utilisateurs que le client se
																						// d�connecte
		}
		try {
			if (cli.isConnected()) {
				cli.close();
				System.out.println("Kill client");
			}
		} catch (IOException e) {// exception catch si l'on arrive pas � deconnecter le client
			System.out.println("Probl�me survenu sur le serveur...");
		}
	}

	/**
	 * Permet d'ajouter le client dans la Hashmap {@link Class_Serv#clients} si le
	 * pseudo choisi n'a pas d�j� �t� utilis�, ceci permet de valider d�finitivement
	 * la connexion du client. Il renvoie vrai si le pseudo du client est accept� et
	 * faux sinon.
	 * 
	 * @param log Pseudo choisi par le client
	 * @param com Socket de communication entre le client et le serveur
	 * @return Faux si le client existe d�j� (m�me pseudo) et vrai sinon.
	 */

	public static boolean check_Id(String log, Socket com) {
		if (clients.containsKey(log))
			return false;
		clients.put(log, com);
		return true;

	}


	/**
	 * Fonction principale permettant la cr�ation de la socket, l'acceptation des connexions clientes et la
	 * cr�ation/lancement des threads serveur desservant un client en particulier.
	 * 
	 * @see MessageReceptor_server
	 * @param args Argument que l'on peut passer au main (aucun ici)
	 */
	public static void main(String[] args) {// dans le main on ne rajoute pas dans la Hashmap le client car on attend
											// que celui-ci s'identifie (voir dans le thread)
		try {
			ServerSocket com = new ServerSocket(20000);
			System.out.println("Cr�ation du serveur...");
			while (true) {
				Socket con = com.accept(); // on accepte les clients voulant se connecter
				MessageReceptor_server msgreceptor = new MessageReceptor_server(con);// on cr�e le thread serveur
																						// desservant le client accept�
				msgreceptor.start();// on lance le thread
				System.out.println("Demande de connexion accept�e....");
			}
		} catch (IOException e) { // socket impossible � cr�er ou probl�me de connexion entre client/serveur
			System.out.println("Probl�me survenu sur le serveur...");
		}

	}


}

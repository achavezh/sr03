package Serveur;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import General.MessageReceptor;

/**
 * 
 * @author C-Emilen <b> Message_Receptor_server est une classe thread h�ritant
 *         de @see MessageReceptor et qui a pour but de r�cup�rer les messages
 *         envoy�s par le client qu'il sert et de les renvoyer aux autres
 *         clients. </b>
 *         <p>
 *         Ce thread diff�re des autres par son traitement en ex�cution, mais
 *         aussi de par ses nouveaux attributs:
 *         <ul>
 *         <li>Un attribut pseudo qui est en fait celui du client qu'il
 *         sert.</li>
 *         <li>L'entr�e de la socket de communication entre le client et le
 *         thread serveur.</li>
 *         </ul>
 *         </p>
 */

public class MessageReceptor_server extends MessageReceptor {

	/**
	 * L'attribut pseudo est le pseudo du client que le thread dessert.
	 */
	private String pseudo = "";

	/**
	 * L'attribut ins est l'entr�e de la socket de communication permettant de lire
	 * les messages venant du client.
	 */
	private DataInputStream ins;

	/**
	 * Constructeur de MessageReceptor_server
	 * 
	 * @param serv com est la socket de connexion/communication entre le client et
	 *             le thread serveur.
	 */
	public MessageReceptor_server(Socket com) {
		super(com);
	}

	/**
	 * Fonction run() utilis�e pour d�finir le traitement effectu� par un thread
	 * MessageReceptor_server en ex�cution.
	 * <p>
	 * Ce traitement consiste � faire plusieurs suite d'instruction r�parties dans
	 * deux boucles tant que le thread n'est pas interrompu.
	 * <ul>
	 * <li>Premi�re boucle: La boucle de la connexion
	 * <ul>
	 * <li>Demander le pseudo du client.</li>
	 * <li>Si le pseudo est �gale � exit, on ferme les ressources de communication
	 * entre le thread serveur et le client puis on termine l'ex�cution.</li>
	 * <li>Si le client n'a rien rentr�, on lui envoie un message et on r�it�re la
	 * boucle.</li>
	 * <li>Si le pseudo est d�j� pris, on lui envoie un message et on r�it�re la
	 * boucle.</li>
	 * <li>Si le pseudo est valide, on diffuse un message pour signaler une nouvelle
	 * connexion � tous les clients et on valide d�finitivement sa connexion
	 * avec @see Class_Serv#check_id(String, Socket)
	 * </ul>
	 * <li>Deuxi�me boucle: La boucle de diffusion des messages
	 * <ul>
	 * <li>Le thread attend un message du client.</li>
	 * <li> Si le message est �gale � exit, on ferme les ressources de communication
	 * entre le thread serveur et le client puis on termine l'ex�cution.</li>
	 * <li> Sinon, on diffuse le message � tous les clients. </li>
	 * </ul>
	 * </ul>
	 * </p>
	 *
	 */

	public void run() {
		try {
			ins = new DataInputStream(com.getInputStream());
			boolean first_Connection = true;
			String chaine = "";
			while (first_Connection) { // tant que l'utilisateur ne s'est pas connect� ou n'a pas exit
				Class_Serv.diffusion("Entrez votre pseudo:", com);
				pseudo = ins.readUTF();
				if (pseudo.equals("exit")) {//si le pseudo est exit on quitte le thread et on lib�re les ressources
					Class_Serv.kill(com, pseudo);
					break;
				} else {
					if (pseudo.equals("")) // si le pseudo n'est pas viable on envoie un message signal et on r�it�re la demande 
						Class_Serv.diffusion("Le pseudo est vide", com);
					else {
						if (Class_Serv.check_id(pseudo, com)) {
							Class_Serv.diffusion("  " + pseudo + " a rejoint la conversation", null);
							Class_Serv.diffusion("------------------------------", com);
							first_Connection = false;
						} else
							Class_Serv.diffusion("Pseudo d�j� pris", com);
					}
				}
			}
			if (first_Connection == false) {
				while (true) {
					chaine = ins.readUTF();
					if (chaine.equals("exit")) {
						Class_Serv.kill(com, pseudo);
						break;
					} else
						Class_Serv.diffusion(pseudo + " a dit: " + chaine, null);
				}
			}
		} catch (SocketException s) {
		} catch (EOFException e) {
		} catch (IOException ex) {
		}
		try {
			ins.close();
		} catch (IOException ex) {
			System.out.println("Probl�me de connexion avec le client...");
		}
		Class_Serv.kill(com, pseudo);
	}

}

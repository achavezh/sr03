package Client;

import java.net.*;
import java.io.*;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Class_Client est la classe repr�santant l'entit� client du chat
 *         multithread, qui se divise en deux threads clients </b>
 *         </p>
 *         <p>
 *         Un client est repr�sent� par les informations suivantes :
 *         <ul>
 *         <li>Un thread interceptant les messages venant du serveur.</li>
 *         <li>Un thread r�cup�rant les messages �crits par le client et les
 *         transmettre au serveur.</li>
 *         <li>Une socket permettant de demander et d'�tablir une connexion avec
 *         le serveur.</li>
 *         <li>Une sortie et une entr�e permettant la communication avec le
 *         serveur.</li>
 *         <li>Un buffer pour r�cup�rer les messages envoy�s par le client sur
 *         l'entr�e standard.</li>
 *         </ul>
 *         <p>
 *         Le client est impl�ment� directement � travers sa fonction main qui
 *         cr�e les deux threads clients et les lance
 *         </p>
 * @see MessageReceptor_client_Envoyer
 * @see MessageReceptor_client_Recevoir
 */

public class Class_Client {

	/**
	 * Constructeur par d�faut de la classe client.
	 */

	public Class_Client() {
	}

	/**
	 * L'attribut com est une socket permettant la connexion et communication avec
	 * le serveur.
	 * 
	 * @see Class_Client#getSock()
	 */
	private static Socket com;
	private static MessageReceptor_client_Recevoir msgReceptorServ;
	private static MessageReceptor_client_Envoyer msgReceptorClient;

	/**
	 * Retourne la socket de communication
	 * 
	 * @return La socket de communication
	 */
	public static Socket getSock() {
		return com;
	}
	
	/**
	 * Change la valeur de l'attribut {@link Class_Client#com}.
	 * @param c Socket de communication entre le client et le serveur.
	 */
	public static void setSock(Socket c) {
		com=c;
	}

	/**
	 * Interrompt les threads client et lib�re les ressources utilis�es.
	 * 
	 * @see Class_Client#msgReceptorClient
	 * @see Class_Client#msgReceptorServ
	 * @see Class_Client#out
	 * @see Class_Client#ins
	 * @see Class_Client#message
	 * @see Class_Client#com
	 */

	public static void tellReceiveThread() {
		msgReceptorServ.setRunning(false);
	}
	
	public static void closeConnection() {
		try {// on lib�re les flux de communication ainsi que la socket pour compl�ter la
				// fermeture du client
			if (com != null && !com.isClosed())
				com.close();
		} catch (IOException e) {
			System.out.println("Probl�me survenu");
		}
	}

	/**
	 * Fonction main permettant la cr�ation et le lancement des threads, la
	 * connexion avec le serveur et la cr�ation/ouverture des flux entre le client
	 * et le serveur.
	 * 
	 * @param args Argument que l'on peut passer au main (aucun ici)
	 */
	public static void main(String[] args) {
		try {
			com = new Socket("localhost", 20000);
			msgReceptorServ = new MessageReceptor_client_Recevoir(com);
			msgReceptorServ.start();
			MessageReceptor_client_Envoyer msgReceptorClient = new MessageReceptor_client_Envoyer(com);
			msgReceptorClient.start();
		} catch (SocketException s) { // si la connexion avec le serveur �choue
			System.out.println("Vous n'avez pas r�ussi � vous connecter");
			closeConnection();
		} catch (IOException e) {
			System.out.println("Probl�me survenu, vous allez �tre d�connect�...");
			closeConnection();
		}
	}

}

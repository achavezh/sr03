package Client;

import java.net.*;
import java.io.*;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Class_Client est la classe repr�santant l'entit� client du chat
 *         multithread, qui se divise en deux threads clients.</b>
 *         </p>
 *         <p>
 *         Un client est repr�sent� par les informations suivantes :
 *         <ul>
 *         <li>Un thread interceptant les messages venant du serveur.</li>
 *         <li>Un thread r�cup�rant les messages �crits par le client et les
 *         transmettre au serveur.</li>
 *         <li>Une socket permettant de demander et d'�tablir une connexion avec
 *         le serveur.</li>
 *         </ul>
 *         <p>
 *         Le client est impl�ment� directement � travers sa fonction main qui
 *         cr�e les deux threads clients et les lance.
 *         </p>
 * @see MessageReceptor_client_Envoyer
 * @see MessageReceptor_client_Recevoir
 */

public class Class_Client {

	/**
	 * Constructeur par d�faut de la classe client.
	 */

	public Class_Client() {
	}

	/**
	 * L'attribut com est une socket permettant la connexion et communication avec
	 * le serveur.
	 * 
	 * @see Class_Client#getSock()
	 */
	private static Socket com;
	private static MessageReceptor_client_Recevoir msgReceptorServ;
	private static MessageReceptor_client_Envoyer msgReceptorClient;

	/**
	 * Retourne la socket de communication.
	 * 
	 * @return La socket de communication.
	 */
	public static Socket getSock() {
		return com;
	}

	/**
	 * Change la valeur de l'attribut {@link Class_Client#com}.
	 * 
	 * @param c Socket de communication entre le client et le serveur.
	 */
	public static void setSock(Socket c) {
		com = c;
	}

	/**
	 * Permet de positionner le boolean running � faux pour que le thread recevant
	 * les messages serveur {@link MessageReceptor_client_Recevoir} termine son ex�cution.
	 * 
	 * @see Class_Client#msgReceptorServ
	 */
	public static void tellReceiveThread() {
		if(msgReceptorServ!=null && msgReceptorServ.isAlive()) //il faut que le thread existe et soit en ex�cution
			msgReceptorServ.setRunning(false);
	}

	/**
	 * Ferme la connexion entre le serveur et le client. La fonction socket.close()
	 * ferme aussi l'entr�e et la sortie de la socket.
	 * 
	 * @see Class_Client#com
	 */
	public static void closeConnection() {
		try {// on lib�re les flux de communication ainsi que la socket pour compl�ter la
				// fermeture du client
			if (com != null && !com.isClosed()) //si la connexion existe et qu'elle n'est pas d�j� ferm�e
				com.close();
		} catch (IOException e) {
			System.out.println("Probl�me survenu");
		}
	}

	/**
	 * Fonction main permettant la cr�ation et le lancement des threads ainsi que la
	 * connexion avec le serveur.
	 * 
	 * @param args Argument que l'on peut passer au main (aucun ici).
	 */
	public static void main(String[] args) {
		try {
			com = new Socket("localhost", 20000); // connexion au serveur
			msgReceptorServ = new MessageReceptor_client_Recevoir(com);
			msgReceptorServ.start();
			msgReceptorClient = new MessageReceptor_client_Envoyer(com);
			msgReceptorClient.start(); // cr�ation et lancement des threads clients
		} catch (SocketException s) { // si la connexion avec le serveur �choue, 
			System.out.println("Vous n'avez pas r�ussi � vous connecter");
			closeConnection();
		} catch (IOException e) {
			System.out.println("Probl�me survenu, vous allez �tre d�connect�...");
			closeConnection();
		}
	}

}

package Client;

import java.net.*;
import java.io.*;

import General.MessageReceptor;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Message_Receptor_client_Recevoir est une classe thread h�ritant
 *         de MessageReceptor et qui a pour but de recevoir et afficher les
 *         messages provenant du serveur. </b>
 *         </p>
 *         <p>
 *         Ce thread diffère des autres par son traitement en exécution.
 *         </p>
 */

public class MessageReceptor_client_Recevoir extends MessageReceptor {

	/**
	 * Constructeur de la classe MessageReceptor_client_Recevoir.
	 * 
	 * @param serv Socket de connexion/communication avec le serveur.
	 */
	public MessageReceptor_client_Recevoir(Socket serv) {
		super(serv);
	}

	/**
	 * L'attribut running est le boolean controlant l'exécution du thread.
	 * {@link MessageReceptor_client_Recevoir}.
	 * 
	 * @see MessageReceptor_client_Recevoir#getRunning()
	 * @see MessageReceptor_client_Recevoir#setRunning(boolean)
	 */
	private volatile boolean running = true; // volatile permet d'�crire dans la m�moire principale et pas sur le
												// cache
	// des threads et donc de garder une coh�rence sur la valeur de la
	// variable tant qu'un seul thread �crit dessus et que l'autre ne fait
	// que de lire.

	/**
	 * L'attribut in est l'entr�e de la socket communication client-serveur.
	 * 
	 * @see MessageReceptor_client_Recevoir#getIn()
	 * @see MessageReceptor_client_Recevoir#setIn(DataInputStream)
	 */

	private DataInputStream in;

	/**
	 * Retourne l'entr�e de la socket de communication client-serveur.
	 * 
	 * @return L'entr�e de la socket de communication client-serveur.
	 */
	public DataInputStream getIn() {
		return in;
	}

	/**
	 * Retourne le boolean controlant l'ex�cution.
	 * 
	 * @return Le boolean controlant l'ex�cution.
	 */
	public boolean getRunning() {
		return running;
	}

	/**
	 * Change la valeur de l'attribut {@link MessageReceptor_client_Recevoir#in}.
	 * 
	 * @param i Entrée de la socket de communication client-serveur.
	 */
	public void setIn(DataInputStream i) {
		in = i;
	}

	/**
	 * Change la valeur de l'attribut running.
	 * 
	 * @param r Boolean contr�lant l'exécution du thread.
	 */
	public void setRunning(boolean r) {
		running = r;
	}

	/**
	 * Fonction run() utilisée pour définir le traitement effectué par un thread
	 * MessageReceptor_client_Recevoir en ex�cution.
	 * <p>
	 * Ce traitement consiste � faire la suite d'instruction suivante en boucle tant
	 * que la condition "doit continuer à s'exécuter" est remplie:
	 * <ul>
	 * <li>Attendre la reception d'un message sur l'entr�e de lecture de la socket
	 * de communication pendant un certain temps d�fini.</li>
	 * <li>Afficher ce message sur la sortie standard.</li>
	 * </ul>
	 * <p>
	 * Pour ce qui est de la gestion de la fermeture du thread , si la condition
	 * "doit continuer � s'ex�cuter" n'est plus remplie, celui-ci s'arr�te
	 * proprement. Cependant, si le serveur s'interrompt sans pr�venir, la connexion
	 * entre le client et le serveur (socket) se ferme (parce que le serveur
	 * s'interrompt) ce qui provoque une exception de type SocketException et
	 * termine l'ex�cution du thread {@link MessageReceptor_client_Recevoir}.
	 * </p>
	 */
	public void run() {
		try {
			in = new DataInputStream(com.getInputStream());
			com.setSoTimeout(1000);
			while (running && !com.isClosed() && com.isConnected()) {
				try {
					String chaine = in.readUTF();// attendre un message en provenance du serveur.
					System.out.println(chaine);// afficher le message
				} catch (SocketTimeoutException to) {
					continue;
				} // exception lanc�e au bout de 1000 ms lorsque rien n'est lu sur l'entr�e de la
					// socket, permet au programme de boucler et continuer d'aller verifier la
					// variable running (condition d'ex�cution "doit continuer � s'ex�cuter"
			}
		} catch (SocketException s) {
			System.out.println("Connexion perdue avec le serveur, vous allez �tre d�connect�...");
			// si la connexion entre le serveur et le client se ferme brusquement (ex:
			// serveur se ferme brusquement), lire sur l'entr�e getIn() provoque une
			// exception SocketException, on �teint l'autre thread client par le biais de
			// closeConnexion() si la socket n'est pas d�j� ferm�e.
		} catch (IOException ex) {
			// si la connexion entre le serveur et le client se ferme brusquement (ex:
			// serveur se ferme brusquement) ou que l'on a un probl�me au niveau de l'entr�e
			// de la socket, on �teint l'autre thread client par le biais de
			// closeConnexion().
		}
		System.out.println("Deconnexion...");
		Class_Client.closeConnexion();// lancer la fonction de fermeture du client.
	}

}

package Client;

import java.net.*;
import java.io.*;

import General.MessageReceptor;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Message_Receptor_client_Recevoir est une classe thread h�ritant
 *         de MessageReceptor et qui a pour but de recevoir et afficher les
 *         messages provenant du serveur. </b>
 *         </p>
 *         <p>
 *         Ce thread diff�re des autres par son traitement en ex�cution.
 *         </p>
 */

public class MessageReceptor_client_Recevoir extends MessageReceptor {

	/**
	 * Constructeur de la classe MessageReceptor_client_Recevoir.
	 * 
	 * @param serv Socket de connexion/communication avec le serveur.
	 */
	public MessageReceptor_client_Recevoir(Socket serv) {
		super(serv);
	}

	/**
	 * L'attribut running est le boolean contr�lant l'ex�cution du thread
	 * {@link MessageReceptor_client_Recevoir}.
	 */
	private static volatile boolean running = true; // volatile permet d'�crire dans la m�moire principale et pas sur le
													// cache
	// des threads et donc de garder une coh�rence sur la valeur de la
	// variable tant qu'un seul thread �crit dessus et que l'autre ne fait
	// que de lire.

	private DataInputStream in;

	/**
	 * Retourne le boolean contr�lant l'ex�cution.
	 * 
	 * @return Le boolean contr�lant l'ex�cution.
	 */
	public static boolean getRunning() {
		return running;
	}

	/**
	 * Change la valeur de l'attribut running.
	 * 
	 * @param r Boolean contr�lant l'ex�cution du thread.
	 */
	public static void setRunning(boolean r) {
		running = r;
	}

	/**
	 * Fonction run() utilis�e pour d�finir le traitement effectu� par un thread
	 * MessageReceptor_client_Recevoir en ex�cution.
	 * <p>
	 * Ce traitement consiste � faire cette suite d'instruction suivante en boucle
	 * tant que le thread n'est pas interrompu.
	 * <ul>
	 * <li>Attendre la reception d'un message sur l'entr�e de lecture de la socket
	 * de communication.</li>
	 * <li>Afficher ce message sur la sortie standard.</li>
	 * </ul>
	 *
	 */
	public void run() {
		try {
			in = new DataInputStream(com.getInputStream());
			com.setSoTimeout(1000);
			while (running) {
				try {
					String chaine = in.readUTF();// attendre un message en provenance du serveur.
					System.out.println(chaine);// afficher le message
				} catch (SocketTimeoutException to) {
					continue;
				} // exception lanc�e au bout de 1000 ms lorsque rien n'est lu sur l'entr�e de la
					// socket, permet au programme de boucler et aller verifier la variable running
			} // pour les messages � afficher en interruption ou en fermeture du client, on
				// laisse le MessageReceptor_client_Envoyer les afficher car via la fonction
				// Class_Client.interrupt(), les deux threads sont li�s. Si l'un se ferme pour
				// n'importe quelle raison, l'autre aussi.
		} catch (SocketException s) {
			System.out.println("Connexion perdue avec le serveur, vous allez �tre d�connect�...");
			// si la connexion entre le serveur et le client se ferme brusquement (ex:
			// serveur se ferme brusquement), lire sur l'entr�e getIn() provoque une
			// exception SocketException, on �teint le client par le biais de
			// Class_Client.interrupt()
		} catch (IOException ex) {
			System.out.println("Probl�me survenu avec le serveur, vous allez �tre d�connect�.");
			// si la connexion entre le serveur et le client se ferme brusquement (ex:
			// serveur se ferme brusquement) ou que l'on a un probl�me au niveau de l'entr�e
			// de la socket, on �teint le client par le biais de Class_Client.interrupt()
		}
		Class_Client.closeConnection();// lancer la fonction de fermeture du client.
	}

}

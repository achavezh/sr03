package Client;

import java.net.Socket;
import java.net.SocketException;
import General.MessageReceptor;

import java.io.*;

/**
 * 
 * @author C-Emilen
 *         <p>
 *         <b> Message_Receptor_client_Envoyer est une classe thread héritant de
 *         MessageReceptor et qui a pour but d'envoyer les messages clients au
 *         serveur. </b>
 *         </p>
 *         <p>
 *         Ce thread diffère des autres par son traitement en ex�cution.
 *         </p>
 */

public class MessageReceptor_client_Envoyer extends MessageReceptor {

	/**
	 * L'attribut out est la sortie de la socket de communication client-serveur.
	 * 
	 * @see MessageReceptor_client_Envoyer#getOut()
	 * @see MessageReceptor_client_Envoyer#setOut(DataOutputStream)
	 */
	private DataOutputStream out;

	/**
	 * L'attribut message est le buffer lecteur permettant de lire l'entrée
	 * standard.
	 * 
	 * @see MessageReceptor_client_Envoyer#getBuff()
	 * @see MessageReceptor_client_Envoyer#setBuff(BufferedReader)
	 */
	private BufferedReader message;

	/**
	 * Constructeur de la classe MessageReceptor_client_Envoyer.
	 * 
	 * @param serv Socket de connexion/communication avec le serveur.
	 */
	public MessageReceptor_client_Envoyer(Socket serv) {
		super(serv);
	}

	/**
	 * Retourne la sortie de la socket de communication client-serveur.
	 * 
	 * @return La sortie de la socket de communication client-serveur.
	 */
	public DataOutputStream getOut() {
		return out;
	}

	/**
	 * Retourne un buffer lecteur permettant la lecture de la sortie standard.
	 * 
	 * @return Un buffer lecteur permettant la lecture de la sortie standard.
	 */

	public BufferedReader getBuff() {
		return message;
	}

	/**
	 * Change la valeur de l'attribut
	 * {@link MessageReceptor_client_Envoyer#message}.
	 * 
	 * @param b Buffer lecteur pour lire l'entr�e standard.
	 */
	public void setBuff(BufferedReader b) {
		message = b;
	}

	/**
	 * Change la valeur de l'attribut
	 * {@link MessageReceptor_client_Envoyer#message}.
	 * 
	 * @param o Sortie de la socket de communication client-serveur.
	 */
	public void setOut(DataOutputStream o) {
		out = o;
	}

	/**
	 * Fonction run() utilisée pour définir le traitement effectu� par un thread
	 * MessageReceptor_client_Envoyer en exécution.
	 * <p>
	 * Ce traitement consiste à faire la suite d'instruction suivante en boucle tant
	 * que la connexion n'est pas interrompue.
	 * <ul>
	 * <li>Espionner si l'entr�e standard est pr�te � �tre lu.</li>
	 * <li>Récupérer le messages envoyé par le client.</li>
	 * <li>Si le message est "exit" on quitte la boucle, on prévient le thread
	 * receveur {@link MessageReceptor_client_Recevoir} ,par le biais d'un boolean,
	 * de l'arrêtt de l'exécution, on ferme le buffer lecteur
	 * {@link MessageReceptor_client_Envoyer#message} puis on arrête
	 * l'exécution.</li>
	 * <li>Sinon, diffuser ce message au server.</li>
	 * </ul>
	 * <p>
	 * Pour ce qui est de la gestion de la fermeture du thread , si celui-ci reçoit
	 * "exit" il se ferme proprement comme expliqué au-dessus. Cependant, si le
	 * serveur s'interrompt sans pr�venir, la connexion entre le client et le
	 * serveur (socket) se ferme (parce que le serveur s'interrompt et/ou gr�ce �
	 * l'appel de {@link Class_Client#closeConnexion()} par
	 * {@link MessageReceptor_client_Recevoir}) ce qui termine l'ex�cution du thread
	 * {@link MessageReceptor_client_Envoyer} (sortie de boucle).
	 * </p>
	 */
	public void run() {
		String ms = "";
		try {
			message = new BufferedReader(new InputStreamReader(System.in));
			out = new DataOutputStream(com.getOutputStream());
			while (!com.isClosed()) {
				if (message.ready()) { // si le buffer contient des �l�ments � lire et qu'il n'est pas
										// ferm�
					ms = message.readLine();// on lit la ligne
					out.writeUTF(ms);
					if (ms.equals("exit"))// si le message est exit on quitte la boucle
						break;
				}
			} // pour les messages � afficher en interruption ou en fermeture du client, on
				// laisse le MessageReceptor_client_Recevoir les afficher car les deux threads
				// sont li�s par leur fin d'ex�cution: en effet si le
				// MessageReceptor_client_Envoyer se ferme par "exit", il pr�vient l'autre
				// thread avec le changement de valeur du boolean, qui se termine � son tour.
		} catch (SocketException s) {// Sinon, si le serveur
			// s'interrompt brusquement, MessageReceptor_client_Envoyer finit son ex�cution
			// car la socket se ferme (gr�ce � l'appel de closeConnexion() dans le code run
			// de MessageReceptor_client_Recevoir et/ou car la socket se ferme lorsque le
			// serveur s'interrompt).
		} catch (IOException e) {
		}
		Class_Client.tellReceiveThread();// pr�venir l'autre thread de l'arr�t de l'ex�cution
		try {
			if (message != null) // on ferme le buffer
				message.close();
		} catch (IOException e) {
			System.out.println("Probl�me survenu");
		}
	}

}